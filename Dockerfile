FROM debian:latest

RUN apt-get update
RUN apt-get install -y g++ make cmake libsdl2-dev git zlib1g-dev \
libbz2-dev libjpeg-dev libfluidsynth-dev libgme-dev libopenal-dev \
libmpg123-dev libsndfile1-dev libgtk-3-dev timidity nasm \
libgl1-mesa-dev tar libsdl1.2-dev libglew-dev pkg-config cmake-data sudo

RUN if getent group ${USER} ; then groupdel ${USER}; fi &&\
    groupadd -g ${GROUP_ID} ${USER} &&\
    useradd -l -u ${GROUP_ID} -g ${USER} ${USER} &&\
    install -d -m 0755 -o ${USER} -g ${USER} /home/${USER} &&\
    usermod -aG sudo ${USER} &&\
    echo "${USER}     ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

USER ${USER}
WORKDIR /home/${USER}

RUN mkdir -p /home/${USER}
RUN mkdir -pv ~/gzdoom_build && \
cd gzdoom_build && \
git clone --verbose  --depth 1 https://github.com/coelckers/gzdoom.git

RUN chown -R ${USER} /home/${USER}

COPY ./compile_gzdoom.sh ./

ENTRYPOINT [ "./compile_gzdoom.sh" ]
