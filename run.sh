#!/bin/bash
user=$(whoami)
gzdoom_build_dir=$(dirname $(realpath $0))/build
echo $gzdoom_build_dir

sed -i -e 's/${GROUP_ID}/1000/g' -e 's/${USER_ID}/1000/g' -e "s/\${USER\}/$user/g" Dockerfile
docker build . -t gzdoom_builder $1
sed -i -e 's/1000/${GROUP_ID}/g' -e 's/1000/${USER_ID}/g' -e "s/$user/\${USER\}/g" Dockerfile

if [ -d ./build ];then
    rm -rf ./build
fi
mkdir -p ./build

docker run --rm \
--name gzdoom_builder \
-v ${gzdoom_build_dir}:/home/$user/gzdoom_build/gzdoom \
gzdoom_builder